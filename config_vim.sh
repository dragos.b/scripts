#!/bin/bash
# Author: Dragos C. Butnariu
# 

directory(){                                                                                ## Create a backup of ~/.vim directory or create a new directory
    if [[ -d ~/.vim ]]                                                                      ## If exists .vim/ create a backup and remove all from .vim
    then                                                                                    
        echo "El directorio ~/.vim ya existe, se va a hacer un backup ~/.vim.bak"
        echo -e "\n..."
        command cp -r ~/.vim ~/.vim.bak
        command rm -rf ~/.vim/*
        echo -e "Directorio .bak creado\n"
    else                                                                                    ## Else create a .vim/
        echo -e "Creación directorio ~/.vim\n"
        command mkdir ~/.vim
        echo -e "Directorio ~/.vim creado correctamente\n"
    fi
    command mkdir ~/.vim/bundle/                                                            ## Create bundle directory
    echo -e "Se va a clonar el repositorio de Vundle.vim\n"
    command  git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim ## Clone vundle.vim repository
    command touch ~/.vim/vundle.vim ~/.vim/plugins.vundle                                   ## Create vundle.vim & plugins.vundle file and add basic configuration for vundle
    echo -e "\nRepositorio clonado con exito"
    echo "
    set nocompatible            
    filetype off        

    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()
    Plugin 'VundleVim/Vundle.vim'

    source ~/.vim/plugins.vundle

    call vundle#end()           
    filetype plugin indent on
    " >> ~/.vim/vundle.vim

}

vimrc() {                                                                                   ## Create backup of ~./vimrc file and create a basic configuration
    if [[ -e ~/.vimrc  ]]                                                                   ## If exists ~/.vimrc create a backup an remove ~/.vimrc
    then 
        echo "Existe el fichero ~/.vimrc y se hará un backup ~/.vimrc.bak"
        command cp ~/.vimrc ~/.vimrc.bak
        command rm ~/.vimrc
        echo "Fichero copiado con exito"
    else                                                                                    ## Else create ~/.vimrc
        echo "Creación fichero ~/.vimrc"
        command touch ~/.vimrc
    fi                                                                                      ## Basic configuration for ~/.vimrc
    echo "                                                                                  
    set nu
    set cindent
    set sw=4
    set expandtab 
    set softtabstop=2
    syntax on
    set t_Co=256
    set syntax=on
    colorscheme elflord
    source ~/.vim/vundle.vim
    " >> ~/.vimrc
}

plugins(){                                                                                  ## Search plugins and install the basics
    read -p "Introduce plugin para buscar: " search_plugin
    command sensible-browser https://vimawesome.com/?q=$search_plugin &                      ## Create a variable for search a plugin in vimawesome.com

}
pause(){                                                                                    ## Pause function similar command pause in MSDOS
    read -p "$*"
} 
pluginstall(){                                                                              ## Install new plugin in ~/.vim/plugins.vundle
    new_plugin=1
    while [[ $new_plugin -eq 1 ]]
    do
        echo -e "Vas a instalar un plugin, introduce la ruta de instalación para Vundle\nEjemplo: Plugin 'vim-airline/vim-airline'\n"
        read ruta
        echo -e "\nHas instalado $ruta\n"
        echo 
        echo $ruta >> ~/.vim/plugins.vundle                                                 ## Add new plugin in ~/.vim/plugins.vundle
        echo -e "1. Añadir otro plugin\n0. Salir\n"
        read -p "Opción: " new_plugin
    done
    echo -e "Se van a instalar los Plugins añadidos anteriormente"
    pause                                                                               ## Call pause() function
    command vim -c :PluginInstall                                                       ## Execute vim and comand :PluginInstall
}

plugdefault(){
    # Install default plugins and basic configuration
    printf "Se van a instalar los siguientes plugins\n1.- Delimitmate\n2.-Syntastic\n3.-Vim-airline\n4.-Indent Guides\n"
    directory
    echo "

    Plugin 'raimondi/delimitmate'
    Plugin 'scrooloose/syntastic'
    Plugin 'vim-airline/vim-airline'
    Plugin 'nathanaelkane/vim-indent-guides'
    " >> ~/.vim/plugins.vundle
    # Default config for syntastic
    echo "


    set statusline+=%#warningmsg#
    set statusline+=%{SyntasticStatuslineFlag()}
    set statusline+=%*

    let g:syntastic_always_populate_loc_list = 1
    let g:syntastic_auto_loc_list = 1
    let g:syntastic_check_on_open = 1
    let g:syntastic_check_on_wq = 0

    let g:airline#extensions#tabline#enabled = 1
    " >> ~/.vimrc
}



command clear
command toilet -froman -F border confvi 
printf "\n¿QUE DESEAS HACER?\n\n"
printf "1 INSTALAR Vundle.vim\n2 BUSCAR PLUGIN\n3 INSTALAR PLUGIN\n4 SALIR\n\n"

while [[ $option -lt 4 ]] || [[ $option -ne 4 ]]                                        ## While the option is less than 4 or different from 4 runs the menu
do                                                                                      ## The menu call the functions 
    echo -e "\n"
    read -p  "Introduce opción: " option                                                ## Input variable for case condition
    echo -e "\n"
    case $option in
        *1* ) directory ;;                                                              ## Call directory() function
        *2* ) plugins ;;                                                                ## Call plugins() function
        *3* ) pluginstall;;                                                             ## Call pluginstall() function
        *4* ) echo "Saliendo...";;
        *) echo -e "\nOpción Icorrecta\n";;
    esac
done
