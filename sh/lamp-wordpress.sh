#!/bin/bash
adduser usuario #Añade el usuario donde se almacena el sitio
apt update && apt upgrade -y
apt install mariadb-client mariadb-server php7.0 php7.0-mysql apache2 libapache2-mod-php7.0 -y
wget https://es.wordpress.org/wordpress-5.0.3-es_ES.tar.gz
tar -xf wordpress-*
mkdir /home/usuario/www
cp -R /root/wordpress /home/usuario/www/wordpress
rm /etc/apache2/sites-available/000-* #Elimina el archivo del sitio por defecto que ofrece Apache
touch /etc/apache2/sites-available/000-default.conf
touch /home/usuario/www/wordpress/.htaccess
	#Configura el VirtualHost de apache
	echo "<VirtualHost *:80>  
		ServerAdmin admin@dragos.com
		ServerName www.dragos.com
		DocumentRoot /home/usuario/www/wordpress
		<Directory /home/usuario/www/wordpress>
			Options Indexes FollowSymLinks
			AllowOverride all
			Require all granted
		</Directory>
		</VirtualHost>" >> /etc/apache2/sites-available/000-default.conf
	#Configura el fichero .htaccces dentro de /home/usuario/www/wordpress
	echo "Options FollowSymLinks
Allow from all
	" >> /home/usuario/www/wordpress/.htaccess
chown -R www-data:www-data /home/usuario/www/wordpress
#Crea BBDD, Usuario, Concede privilegios, Flushea Privilegios 
	read -p 'Nombre de la base de datos: ' DB_NAME #Variable Input para le nombre de la BBDD
	read -p 'Nombre de usuario para la BBDD: ' DB_USER #Variable Input para el User de la BBDD
	read -sp 'Contraseña: ' DB_PASSW
mysql -u root -e "CREATE DATABASE $DB_NAME;"	
mysql -u root -e "CREATE USER $DB_USER IDENTIFIED BY '$DB_PASSW';"
mysql -u root -e "GRANT ALL PRIVILEGES ON $DB_NAME.* to $DB_USER;"
mysql -u root -e "FLUSH PRIVILEGES;"
apt install phpmyadmin -y
service apache2 restart
service mysql restart
