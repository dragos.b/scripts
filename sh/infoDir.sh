#!/bin/bash
#Script que recibe la ruta absoluta y lista todo que comience por una vocal y el total de ficheros
echo "Indica la ruta absoluta" 
read -p "Ruta:" r_absoluta
echo " "
echo "Cantidad total ficheros"
ls -la $r_absoluta | wc -l
echo " "
echo "Listado todo lo que comienza por vocal"
ls -d ${r_absoluta}[aeiou]*
