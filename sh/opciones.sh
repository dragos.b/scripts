#!/bin/bash

# Copia el contenido de fichero1 a fichero2
# Comprueba si existe el fichero1 y comprueba que no exista el fichero2
# Se ejecuta el script mediante argumentos ./opciones.sh fichero1 fichero2

if [[ -a $1 ]] #Comprueba si existe fichero del que copiar
then
    if [[ -a $2  ]] # Si existe el fichero1 ejecuta otro if que comprueba
                    # si existe el fichero al que copiar, si no existe 
                    # ejecuta cat con un volcado al fichero2
    then 
        printf "El fichero $2 ya existe\n"
    else
        cat $1 >> ./$2
        printf "Se ha copiado el contenido de $1 a $2\n"
    fi
else
    printf "El fichero $1 no existe\n"
fi

