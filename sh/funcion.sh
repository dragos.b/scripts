#!/bin/bash

# Muestra todas las palabras que comienzan por A del archivo $1

listado () {
    listado=$(grep -E '\bA' $1)
    printf "$listado"
}
listado $1
