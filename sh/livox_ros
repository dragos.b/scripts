#!/bin/bash

#####################################
#####################################
####                           ######
####   Install LivoxSDK, ROS,  ######
####   Livox ROS Driver,       ######
####   Livox Mapping and all   ######
####   dependecies             ######
####                           ######
#####################################
#####################################


# Global Variables

LIVOX_SDK_REPO=https://github.com/Livox-SDK/Livox-SDK.git
LIVOX_DRIVER_REPO=https://github.com/Livox-SDK/livox_ros_driver.git
LIVOX_MAPP=https://github.com/Livox-SDK/livox_mapping.git

UBUNTU_VER=$(lsb_release -rs)
WS_LIVOX=ws_livox         # Livox Workspace on ROS
HOME_DIR=/home/$USER



getStatus() {
	if [ $1 -eq 0 ]; then
		local RV=CORRECTO
	else
		local RV=INCORRECTO
	fi

	echo "$RV"
}

## Ubuntu 16.04 -> kinetic
## Ubuntu 18.04 -> melodic


if [ $UBUNTU_VER = "18.04" ]
then
	ROS_VERS=melodic
elif [ $UBUNTU_VER = "16.04" ]
then
	ROS_VERS=kinetic
fi


ROS_PCKG=ros-$ROS_VERS-desktop-full


sudo apt update && sudo apt upgrade -y
sudo apt install   \
	cmake          \
	curl           \
	git            \
	libpcl-dev     \
	libeigen3-dev  \
	eigensoft -y 

cd $HOME_DIR


# Clone Livox SDK, compile and install.
git clone $LIVOX_SDK_REPO
cd Livox-SDK

if [ ! -d ./build ]
then
	mkdir build
fi

cd build && cmake ..
make
sudo make install

ST_SDK_INST=$(getStatus $?)

# ROS Install
cd $HOME_DIR
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | sudo apt-key add -

sudo apt update && sudo apt install $ROS_PCKG -y

# ROS environment setup
echo "source /opt/ros/$ROS_VERS/setup.bash" >> $HOME_DIR/.bashrc
source $HOME_DIR/.bashrc


sudo apt install                \
	python-rosdep               \
	python-rosinstall           \
	python-rosinstall-generator \
	python-wstool               \
	build-essential -y

# Livox ROS Driver
cd $HOME_DIR
git clone $LIVOX_DRIVER_REPO $WS_LIVOX/src

ST_DRIVER_CLN=$(getStatus $?)

source $HOME_DIR/.bashrc
cd $HOME_DIR/$WS_LIVOX
catkin_make

ST_DRIVER_INST=$(getStatus $?)

# Livox Mapping
cd $HOME_DIR/$WS_LIVOX/src
git clone $LIVOX_MAPP


ST_MAPP_CLN=$(getStatus $?)

cd ..
catkin_make

ST_MAPP_INST=$(getStatus $?)

source $HOME_DIR/.bashrc
cd $HOME_DIR/$WS_LIVOX


sudo apt install ros-$ROS_VERS-imu-tools -y


echo "source $HOME_DIR/$WS_LIVOX/devel/setup.sh"        >> $HOME_DIR/.bashrc
echo "source $HOME_DIR/$WS_LIVOX/devel/setup.bash"      >> $HOME_DIR/.bashrc
echo alias cdlm="'cd ~/ws_livox/src/livox_mapping'"     >> $HOME_DIR/.bashrc
echo alias cdld="'cd ~/ws_livox/src/livox_ros_driver'"  >> $HOME_DIR/.bashrc



MID_SH_FILE=mid-40.sh
HORIZON_SH_FILE=horizon.sh

cd $HOME_DIR
touch $MID_SH_FILE $HORIZON_SH_FILE
echo '#!/bin/bash' > $HOME_DIR/$MID_SH_FILE
echo '#!/bin/bash' > $HOME_DIR/$HORIZON_SH_FILE



echo -e "\n\n\tInstalación SDK =\t\t"   $ST_SDK_INST    \
	"\n\tClonación del Driver =\t\t"    $ST_DRIVER_CLN  \
	"\n\tInstalación del Driver =\t\t"  $ST_DRIVER_INST \
	"\n\tClonación del Mapping =\t\t"   $ST_MAPP_CLN    \
	"\n\tInstalación del Mapping =\t\t" $ST_MAPP_INST

echo
