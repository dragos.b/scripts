class Counter:

    def __init__(self, file_path):
        self.data = dict()
        try:
            self.open_file = open(file_path, 'rt', encoding="utf8")
        except IOError:
            print('The file doesn\'t exists')

    def save_data(self):
        for line in self.open_file:

            init_phone = line.find("-")
            end_phone  = line.find(":", init_phone)

            if init_phone < end_phone:
                complete_phone = line[init_phone+2:end_phone]

                if complete_phone in self.data:
                    count = self.data.get(complete_phone)
                    self.data[complete_phone] = count + 1

                else:
                    self.data.update({complete_phone: 1})
        self.open_file.close()

    def return_data(self):
        data_return = ''
        data_sorted = sorted(self.data.items(), key=lambda x: x[1], reverse=True)
        index = 1
        for i in data_sorted:
            data_return += '%i.- %s : %i\n' % (index, i[0], i[1])
            index += 1

        return data_return
